## Resumen

Este proyecto se plantea con el objetivo fomentar el estudio en grupo, conociendo los beneficios y las aportaciones que este tipo de aprendizaje realiza sobre los estudiantes.

Para ello, se propone el desarrollo de un prototipo de aplicación móvil mediante la cual los usuarios serán capaces de gestionar sus grupos de estudio y participar en un programa de apadrinamiento
en el que un estudiante mas experimentado podrá ayudar a estudiantes de nuevo ingreso en temas tanto académicos como de gestión de trámites de la universidad.

Cada grupo cuenta con un muro en el que los integrantes podrán enviar mensajes comunes a todos los miembros del grupo, así como un repositorio de documentos privado.

